import { request } from "https";

import { getVariable } from "../js/common/config.js";

function getUser( token ){
	return new Promise( ( res ) => {
		let resBody = [];
		let req = request(
			"https://gitlab.com/api/v4/user",
			{
				"method": "GET",
				"headers": {
					"Accept": "application/json",
					"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
					"Authorization": `Bearer ${token}`
				}
			},
			( result ) => {
				result.on( "data", ( data ) => {
					resBody.push( data );
				} );
				result.on( "end", () => {
					resBody = JSON.parse( Buffer.concat( resBody ).toString() );

					res( resBody );
				} );
			}
		);

		req.end();
	} );
}

async function getCurrentFeedback(){
	var snippetId = getVariable( { "type": "config", "name": "snippetId" } );
	var feedbackFile = getVariable( { "type": "config", "name": "feedbackFile" } );

	return new Promise( ( res ) => {
		let resBody = [];
		let req = request(
			`https://gitlab.com/api/v4/snippets/${snippetId}/files/main/${feedbackFile}/raw`,
			{
				"method": "GET",
				"headers": {
					"Accept": "application/json",
					"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
					"PRIVATE-TOKEN": getVariable( { "type": "config", "name": "snippetTokenVar", "recurse": true } )
				}
			},
			( result ) => {
				result.on( "data", ( data ) => {
					resBody.push( data );
				} );
				result.on( "end", () => {
					resBody = Buffer.concat( resBody ).toString();

					res( resBody );
				} );
			}
		);

		req.end();
	} );
}

async function updateFeedback( newFeedback ){
	var snippetId = getVariable( { "type": "config", "name": "snippetId" } );
	var body = {
		"files": [
			{
				"action": "update",
				"file_path": getVariable( { "type": "config", "name": "feedbackFile" } ),
				"content": newFeedback
			}
		]
	};
	var bodyString = JSON.stringify( body );

	return new Promise( ( res ) => {
		let resBody = [];
		let req = request(
			`https://gitlab.com/api/v4/snippets/${snippetId}`,
			{
				"method": "PUT",
				"body": bodyString,
				"headers": {
					"Accept": "application/json",
					"Content-Type": "application/json",
					"PRIVATE-TOKEN": getVariable( { "type": "config", "name": "snippetTokenVar", "recurse": true } )
				}
			},
			( result ) => {
				result.on( "data", ( data ) => {
					resBody.push( data );
				} );
				result.on( "end", () => {
					resBody = JSON.parse( Buffer.concat( resBody ).toString() );

					res( resBody );
				} );
			}
		);

		req.write( bodyString, "utf8" );
		req.end();
	} );
}

async function addFeedback( content ){
	var currentFeedback = await getCurrentFeedback();
	var updatedFeedback = currentFeedback + content;

	return await updateFeedback( updatedFeedback );
}

function isGitLabTeamMember( user ){
	var isMember = user.email.endsWith( "@gitlab.com" ) && user.confirmed_at;
	var otherAddress = getVariable( { "type": "config", "name": "extraEmails" } ).includes( user.email );

	return isMember || otherAddress;
}

function craftFeedback( content, userSlug ){
	var now = new Date();

	return `

============================
At ${now.toISOString()}, ${userSlug} wrote:
----------------------------

${content}
`;
}

async function allowInternalOnly( content, anonymous, user ){
	var code = 403; // Forbidden. You logged in fine, but I refuse to acknowledge this submission
	var response = JSON.stringify( { "error": "Not a GitLab Team Member." } );
	var userSlug = anonymous ? "an anonymous person" : `${user.name} (${user.email})`;

	if( isGitLabTeamMember( user ) ){
		content = craftFeedback( content, userSlug );

		await addFeedback( content );

		code = 200;
		response = JSON.stringify( {
			"submitted": true,
			content
		} );
	}

	return {
		"statusCode": code,
		"body": response
	};
}

export async function handler( event ){
	var { content, anonymous, token } = JSON.parse( event.body );
	var user = await getUser( token );

	return allowInternalOnly( content, anonymous, user );
}
