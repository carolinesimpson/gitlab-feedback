import { request } from "https";

import { getVariable } from "../js/common/config.js";

export async function handler( event ){
	var body = JSON.parse( event.body );
	var config = {
		"url": "https://gitlab.com/oauth/token",
		"postBody": {
			"client_id": getVariable( { "type": "config", "name": "appId" } ),
			"client_secret": getVariable( { "type": "config", "name": "appSecretVar", "recurse": true } ),
			"code": body.code,
			"grant_type": "authorization_code",
			"redirect_uri": body.redirect
		}
	};
	var status = 400;
	var responseBody = JSON.stringify( { "error": "Unknown provider" } );
	var auth = await new Promise( ( res ) => {
		let resBody = [];
		let req = request(
			config.url,
			{
				"method": "POST",
				"body": JSON.stringify( config.postBody ),
				"headers": {
					"Accept": "application/json",
					"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
				}
			},
			( result ) => {
				result.on( "data", ( data ) => {
					resBody.push( data );
				} );
				result.on( "end", () => {
					resBody = JSON.parse( Buffer.concat( resBody ).toString() );

					res( resBody );
				} );
			}
		);

		req.write( ( new URLSearchParams( config.postBody ) ).toString(), "utf8" );
		req.end();
	} );

	status = 200;
	responseBody = JSON.stringify( auth );

	return {
		"statusCode": status,
		"body": responseBody
	};
}
