import { get, set, remove } from "../modules/@thomasrandolph/taproot/storage/local.js";

import { localLogout, upgradeCode } from "../common/oauth.js";

import { message as AUTH_LOGOUT } from "../messages/AUTH_LOGOUT.js";
import { message as AUTH_UPGRADE_OAUTH } from "../messages/AUTH_UPGRADE_OAUTH.js";
import { message as AUTH_OAUTH_FAILURE } from "../messages/AUTH_OAUTH_FAILURE.js";

export function register( { publish, subscribeWith } = {} ){
	subscribeWith( {
		[AUTH_LOGOUT.name]: () => {
			localLogout();

			window.location.reload();
		},
		[AUTH_UPGRADE_OAUTH.name]: async () => {
			var params = new URLSearchParams( window.location.search );
			var state = get( "state" );
			var access;

			if( state ){
				try{
					access = await upgradeCode( params );

					set( "token", access.access_token );
					remove( "state" );

					window.location.reload();
				}
				catch( e ){
					publish( { ...AUTH_OAUTH_FAILURE, "code": params.code, "state": get( "state" ) } );
				}
			}
		}
	} );
}
