import { css } from "../modules/@thomasrandolph/taproot/Component.js";

export function base(){
	return css`

	:host{
		box-sizing: border-box;
	}

	*{
		box-sizing: inherit;
	}

	button{
		padding: 0.5rem 1rem;
	}

	`;
}
