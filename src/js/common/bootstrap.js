import { setup } from "../modules/@thomasrandolph/taproot.js";

import { getVariable } from "./config.js";

import { register as registerAuthActions } from "../actions/Auth.js";
import { register as registerFeedbackActions } from "../actions/Feedback.js";

export async function appInstance(){
	var app = await setup( {
		"namespace": getVariable( { "type": "config", "name": "namespace" } )
	} );

	registerAuthActions( { ...app } );
	registerFeedbackActions( { ...app } );

	return app;
}
