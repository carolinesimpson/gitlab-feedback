import { get } from "../modules/@thomasrandolph/taproot/storage/local.js";

export function isAuthenticated(){
	return get( "token" );
}
