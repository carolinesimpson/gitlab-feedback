import { html } from "../../modules/@thomasrandolph/taproot/Component.js";

export function template( {
	initiate,
	logout,
	authenticated
} = {} ){
	return authenticated
		? signout( { logout } )
		: signin( { initiate } );
}

function signin( { initiate } = {} ){
	return html`

	<div unauthenticated>
		<button @click="${initiate}" login>
			🦊 Sign in with GitLab
		</button>
		<section>
			<h4>Why should I authenticate? I want to send anonymous feedback!</h4>
			<p>
				All feedback is anonymous by default, although you can choose to name yourself before you submit.
			</p>
			<p>
				There is one reason you <strong>must</strong> authenticate: this feedback is for GitLab team members only, so your authentication will be checked to ensure that's you.
			</p>
			<p>
				If you choose to name yourself on the next screen, this same authentication will be used for that.
			</p>
			<p>
				There's no need to trust me. <a href="https://gitlab.com/rockerest/gitlab-feedback/-/blob/main/src/functions/submitFeedback.js" target="_blank">The code is public</a>.
			</p>
		</section>
	</div>

	`;
}

function signout( { logout } = {} ){
	return html`

	<div authenticated>
		<button @click="${logout}" logout>Log Out</button>
	</div>

	`;
}
