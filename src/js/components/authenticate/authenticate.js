import { subscribeWith, publish } from "../../modules/@thomasrandolph/taproot/MessageBus.js";

import { BaseComponent } from "../BaseComponent.js";

import { isAuthenticated } from "../../common/authentication.js";
import { thirdPartyLogin } from "../../common/oauth.js";

import { message as AUTH_OAUTH_FAILURE } from "../../messages/AUTH_OAUTH_FAILURE.js";
import { message as AUTH_UPGRADE_OAUTH } from "../../messages/AUTH_UPGRADE_OAUTH.js";
import { message as AUTH_LOGOUT } from "../../messages/AUTH_LOGOUT.js";

import { template } from "./authenticate.template.js";
import { styles } from "./authenticate.styles.js";
import {
	getState,
	FAILURE_EVENT
} from "./authenticate.state.js";

export class Authenticate extends BaseComponent{
	static get as(){
		return `${BaseComponent.prefix}-authenticate`;
	}

	static get styles(){
		return [
			...super.styles,
			styles()
		];
	}

	static get properties(){
		return {};
	}

	constructor( ...args ){
		super( ...args );

		this.subscriptions = [];

		getState( { "component": this } );
	}

	connectedCallback( ...args ){
		super.connectedCallback( ...args );

		this.subscriptions.push( subscribeWith( {
			[AUTH_OAUTH_FAILURE]: () => {
				this.stateService.transition( FAILURE_EVENT );
			}
		} ) );

		publish( { ...AUTH_UPGRADE_OAUTH } );
	}
	disconnectedCallback( ...args ){
		this.subscriptions.forEach( ( subscription ) => {
			subscription.unsubscribe();
		} );

		super.disconnectedCallback( ...args );
	}

	render(){
		return template( {
			"authenticated": isAuthenticated(),
			"initiate": () => thirdPartyLogin(),
			"logout": () => publish( { ...AUTH_LOGOUT } )
		} );
	}
}
