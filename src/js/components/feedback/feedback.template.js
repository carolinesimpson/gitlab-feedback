import { html } from "../../modules/@thomasrandolph/taproot/Component.js";

export function template( {
	submit
} = {} ){
	return html`

	<div>
		<textarea id="content" placeholder="Celebrations, opportunities for growth, or anything at all on your mind!"></textarea>
		<input type="checkbox" id="deanonymize" />
		<label for="deanonymize">Submit my name with this feedback</label>
		<button @click="${submit}">Send feedback</submit>
	</div>

	`;
}
