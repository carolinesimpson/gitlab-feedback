import { publish } from "../../modules/@thomasrandolph/taproot/MessageBus.js";

import { BaseComponent } from "../BaseComponent.js";

import { message as FEEDBACK_SEND } from "../../messages/FEEDBACK_SEND.js";

import { template } from "./feedback.template.js";
import { styles } from "./feedback.styles.js";
import { getState } from "./feedback.state.js";

export class Feedback extends BaseComponent{
	static get as(){
		return `${BaseComponent.prefix}-feedback`;
	}

	static get styles(){
		return [
			...super.styles,
			styles()
		];
	}

	static get properties(){
		return {};
	}

	constructor( ...args ){
		super( ...args );

		getState( { "component": this } );

		this.content = null;
		this.deanonymize = null;
	}

	connectedCallback( ...args ){
		super.connectedCallback( ...args );
	}
	firstUpdated(){
		this.content = this.shadowRoot.getElementById( "content" );
		this.deanonymize = this.shadowRoot.getElementById( "deanonymize" );
	}
	disconnectedCallback(){}

	render(){
		return template( {
			"submit": this.submit
		} );
	}

	submit(){
		publish( {
			...FEEDBACK_SEND,
			"content": this.content.value,
			"anonymous": !this.deanonymize.checked
		} );
	}
}
