function get( storage, key ){
	return storage.getItem( key );
}

function set( storage, key, val ){
	return storage.setItem( key, val );
}

function remove( storage, key ){
	return storage.removeItem( key );
}

function get$1( key ){
	return get( localStorage, key );
}

function set$1( key, val ){
	return set( localStorage, key, val );
}

function remove$1( key ){
	return remove( localStorage, key );
}

export { get$1 as get, remove$1 as remove, set$1 as set };
