import { b as get, a as getTaproot } from '../../common/requirements-47c762e5.js';
import { n as namespace } from '../../common/MessageBus-8f610afa.js';
export { n as namespace, o as optionOrNoop, t as transaction } from '../../common/MessageBus-8f610afa.js';
import '../../common/Random-ec01f07a.js';
import './Function.js';

var knownProperties = [ "subscribeWith", "publish", "once", "sequence" ];

function publish( message ){
	return get( {
		"name": "publish",
		"properties": knownProperties,
		"scope": "bus",
		"taproot": getTaproot()
	} )( message );
}

function subscribeWith( map ){
	return get( {
		"name": "subscribeWith",
		"properties": knownProperties,
		"scope": "bus",
		"taproot": getTaproot()
	} )( map );
}

function once( name, handler ){
	return get( {
		"name": "once",
		"properties": knownProperties,
		"scope": "bus",
		"taproot": getTaproot()
	} )( name, handler );
}

function sequence( seqConfig ){
	return get( {
		"name": "sequence",
		"properties": knownProperties,
		"scope": "bus",
		"taproot": getTaproot()
	} )( seqConfig );
}

function filterInternalMessages( message ){
	// [].filter( filterInternalMessages )
	// Will filter out all Taproot messages

	var prefix = namespace( { "name": "" }, getTaproot().taproot.namespace ).name;

	return !String( message.name ).startsWith( prefix );
}

export { filterInternalMessages, once, publish, sequence, subscribeWith };
