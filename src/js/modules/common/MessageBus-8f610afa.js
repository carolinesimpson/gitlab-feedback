import { u as uuid } from './Random-ec01f07a.js';
import { pipe } from '../@thomasrandolph/taproot/Function.js';

function noop(){}

function getUnhandledFallbackListeners( listeners ){
	return listeners[ "*" ] || [];
}

function getEveryListeners( listeners ){
	return listeners[ "**" ] || [];
}

function getEventNameFromMessage( message ){
	return typeof message == "string" ? message : message.name;
}

function getListenersForEventName( listeners, name ){
	var base = listeners[ name ] ? listeners[ name ] : [ ...getUnhandledFallbackListeners( listeners ) ];

	return [ ...base, ...getEveryListeners( listeners ) ];
}

function getHandlersForListeners( handlers, listenerIds ){
	return listenerIds.map( ( id ) => handlers[ id ] );
}

function getUnsubscribers( unsubscribe = {} ){
	let normalizedUnsubscribers = {};

	if( unsubscribe instanceof Array ){
		unsubscribe = {
			"before": unsubscribe
		};
	}

	return Object.assign(
		normalizedUnsubscribers,
		{
			"before": [],
			"after": []
		},
		unsubscribe
	);
}

function getTx( txOrMessage, previousTx ){
	let tx = previousTx;

	if( txOrMessage?.name ){
		tx = txOrMessage.tx;
	}
	else if( typeof txOrMessage == "string" ){
		tx = txOrMessage;
	}

	return tx;
}

function attachMethods( busInstance ){
	busInstance.publish = ( function publish( msg ){
		var newMessage;

		if( typeof msg == "string" ){
			newMessage = {
				...transaction(),
				"name": msg
			};
		}
		else {
			newMessage = { ...transaction( msg ), ...msg };
		}

		this.bus.next( newMessage );

		return newMessage;
	} ).bind( busInstance );

	busInstance.subscribeWith = ( function subscribeWith( map ){
		var names = Object.keys( map );
		var handlers = Object.values( map );
		var idMap = [];

		handlers.forEach( ( handler, i ) => {
			let name = names[ i ];
			let id = uuid();

			this.handlers[ id ] = handler;
			if( this.listeners[ name ] ){
				this.listeners[ name ].push( id );
			}
			else {
				this.listeners[ name ] = [ id ];
			}
			idMap.push( { name, id } );
		} );

		return {
			unsubscribe(){
				idMap.forEach( ( { name, id } ) => {
					delete busInstance.handlers[ id ];

					if( busInstance.listeners[ name ] ){
						busInstance.listeners[ name ] = busInstance.listeners[ name ].filter( ( listener ) => listener != id );
					}

					if( busInstance.listeners[ name ].length == 0 ){
						delete busInstance.listeners[ name ];
					}
				} );
			}
		};
	} ).bind( busInstance );

	busInstance.once = ( function once( messageName, handler ){
		var stream;
		var map = {
			[messageName]: ( ...args ) => {
				handler( ...args );

				stream.unsubscribe();
			}
		};

		stream = this.subscribeWith( map );

		return stream;
	} ).bind( busInstance );

	busInstance.sequence = ( function sequence( sequenceConfiguration ){
		var bus = this;
		var { initial, stages } = sequenceConfiguration;
		var listeners = {};
		var tx;

		return {
			unsubscribe(){
				Object
					.values( listeners )
					.forEach( ( { unsubscribe } ) => unsubscribe() );
			},
			trigger(){
				Object
					.entries( stages )
					.forEach( ( [ stageName, stageConfiguration ] ) => {
						let { name, handler, unsubscribe } = stageConfiguration;
						let { before, after } = getUnsubscribers( unsubscribe );

						listeners[ stageName ] = bus.once( name, ( message ) => {
							if( message.tx == tx ){
								before.forEach( ( unsub ) => {
									listeners[ unsub ]?.unsubscribe();
								} );

								tx = getTx( handler( message ), tx );

								after.forEach( ( unsub ) => {
									listeners[ unsub ]?.unsubscribe();
								} );
							}
						} );
					} );

				tx = bus.publish( initial ).tx;
			}
		};
	} ).bind( busInstance );
}

function Bus( settings = {}, ...args ){
	var self = this;

	function getProxyBus(){
		var active = true;
		var stream = [];
		var subscribers = [];
		var bus = new Proxy( stream, {
			"get": ( target, name ) => {
				let prox = [ "subscribe", "next", "drain", "activate", "deactivate", "start", "stop" ];
				let handlers = {
					"subscribe": ( subscriber ) => {
						subscribers.push( subscriber );
					},
					"next": ( ...values ) => {
						bus.push( ...values );
					},
					"drain": () => {
						if( active ){
							while( stream.length ){
								let message = stream.shift();

								subscribers.forEach( ( sub ) => sub( message ) );
							}
						}
					},
					"activate": () => active = true,
					"deactivate": () => active = false,
					"start": () => {
						bus.activate();
						bus.drain();
					},
					"stop": () => bus.deactivate()
				};
				let response;

				if( prox.includes( name ) ){
					response = handlers[ name ];
				}
				else {
					response = Reflect.get( target, name );

					if( name === "push" ){
						setTimeout( () => {
							bus.drain();
						}, 0 );
					}
				}

				return response;
			}
		} );

		return bus;
	}

	if( !( this instanceof Bus ) ){
		self = new Bus( settings, ...args );
	}
	else {
		this.listeners = {};
		this.handlers = {};
		this.bus = getProxyBus();

		this.bus.subscribe( ( event ) => {
			pipe(
				event,
				getEventNameFromMessage,
				( name ) => getListenersForEventName( this.listeners, name ),
				( listeners ) => getHandlersForListeners( this.handlers, listeners ),
				( handlers ) => {
					handlers.forEach( ( handler ) => {
						handler( event );
					} );
				}
			);
		} );

		attachMethods( this );
	}

	return self;
}

function create( ...args ){
	return new Bus( ...args );
}

function transaction( message = {} ){
	var tx = message.tx ? message.tx : uuid();

	return { tx };
}

function namespace( message, ns ){
	return {
		...message,
		"name": `${ns}|${message.name}`
	};
}

function optionOrNoop( options, optName ){
	return options[ optName ] || noop;
}

export { create as c, namespace as n, optionOrNoop as o, transaction as t };
