import { c as clamp } from './Random-ec01f07a.js';

function log( ...args ){
	console.log( ...args ); // eslint-disable-line no-console
}

function error( ...args ){
	console.error( ...args ); // eslint-disable-line no-console
}

function emit( message, level = 3 ){
	var handlers = [
		() => {},
		error,
		( msg ) => {
			throw new Error( msg );
		}
	];

	level = clamp( level, 1, 3 ) - 1;

	handlers[ level ]( message );
}

const DEFAULT_NAMESPACE = "taproot";
var defaultTaproot = {
	"ready": false,
	"namespace": "TAPROOT",
	"errorLevel": 2
};

function namespaceExists(){
	return Boolean( window.NAMESPACE );
}

function taprootExists( namespace ){
	return Boolean( window[ namespace ] );
}

function ensureKnownProperty( name, properties, scope, errorLevel ){
	if( !properties.includes( name ) ){
		emit(
			`Unaware of any ${scope} property '${name}'. Valid options are ${JSON.stringify( properties )}`,
			errorLevel
		);
	}
}

function ensureTaprootProperty( name, taproot ){
	if( !taproot[ name ] ){
		emit( `Can't get '${name}' from the root object @ ${getNamespace()}. Have you already setup Taproot?`, taproot.errorLevel );
	}
}

function getNamespace(){
	return namespaceExists() ? window.NAMESPACE : DEFAULT_NAMESPACE;
}

function getTaproot(){
	var namespace = getNamespace();

	return taprootExists( namespace ) ? window[ namespace ] : defaultTaproot;
}

function get( { properties, scope, name, taproot } ){
	ensureKnownProperty( name, properties, scope, taproot.errorLevel );
	ensureTaprootProperty( name, taproot );

	return taproot[ name ] || ( () => {} );
}

export { getTaproot as a, get as b, getNamespace as g, log as l };
