const N = 624;
const N_MINUS_1 = 623;
const M = 397;
const M_MINUS_1 = 396;
const DIFF = N - M;
const MATRIX_A = 0x9908b0df;
const UPPER_MASK = 0x80000000;
const LOWER_MASK = 0x7fffffff;

function twist( state ){
	var bits;

	// first 624-397=227 words
	for( let i = 0; i < DIFF; i++ ){
		bits = ( state[i] & UPPER_MASK ) | ( state[i + 1] & LOWER_MASK );

		state[i] = state[i + M] ^ ( bits >>> 1 ) ^ ( ( bits & 1 ) * MATRIX_A );
	}
	// remaining words (except the very last one)
	for( let i = DIFF ; i < N_MINUS_1; i++ ){
		bits = ( state[i] & UPPER_MASK ) | ( state[i + 1] & LOWER_MASK );

		state[i] = state[i - DIFF] ^ ( bits >>> 1 ) ^ ( ( bits & 1 ) * MATRIX_A );
	}

	// last word is computed pretty much the same way, but i + 1 must wrap around to 0
	bits = ( state[N_MINUS_1] & UPPER_MASK ) | ( state[0] & LOWER_MASK );

	state[N_MINUS_1] = state[M_MINUS_1] ^ ( bits >>> 1 ) ^ ( ( bits & 1 ) * MATRIX_A );

	return state;
}

/* eslint-disable complexity */
function initializeWithArray( seedArray ){
	var state = initializeWithNumber( 19650218 );
	var len = seedArray.length;

	var i = 1;
	var j = 0;
	var k = ( N > len ? N : len );

	for( ; k; k-- ){
		let s = state[i - 1] ^ ( state[i - 1] >>> 30 );

		state[i] = (
			state[i] ^ (
				(
					(
						(
							( s & 0xffff0000 ) >>> 16
						) * 1664525
					) << 16
				) +
				(
					( s & 0x0000ffff ) * 1664525
				)
			)
		) + seedArray[j] + j;
		i++; j++;
		if( i >= N ){
			state[0] = state[N_MINUS_1]; i = 1;
		}
		if( j >= len ){
			j = 0;
		}
	}
	for( k = N_MINUS_1; k; k-- ){
		let s = state[i - 1] ^ ( state[i - 1] >>> 30 );

		state[i] = (
			state[i] ^ (
				(
					(
						(
							( s & 0xffff0000 ) >>> 16
						) * 1566083941
					) << 16
				) +
				( s & 0x0000ffff ) * 1566083941
			)
		) - i;
		i++;
		if( i >= N ){
			state[0] = state[N_MINUS_1]; i = 1;
		}
	}

	state[0] = UPPER_MASK; /* MSB is 1; assuring non-zero initial array */

	return state;
}

function initializeWithNumber( seed ){
	var state = new Array( N );

	// fill initial state
	state[0] = seed;
	for( let i = 1; i < N; i++ ){
		let s = state[i - 1] ^ ( state[i - 1] >>> 30 );
		// avoid multiplication overflow: split 32 bits into 2x 16 bits and process them individually

		state[i]  = (
			(
				(
					(
						( s & 0xffff0000 ) >>> 16
					) * 1812433253
				) << 16
			) + ( s & 0x0000ffff ) * 1812433253
		) + i;
	}

	return state;
}

// The original algorithm used 5489 as the default seed
function initialize( seed = Date.now() ){
	var state;

	if( Array.isArray( seed ) ){
		state = initializeWithArray( seed );
	}
	else {
		state = initializeWithNumber( seed );
	}

	return twist( state );
}

function MersenneTwister( seed ){
	var state = initialize( seed );
	var next = 0;
	var randomInt32 = () => {
		let x;

		if( next >= N ){
			state = twist( state );
			next = 0;
		}

		x = state[ next++ ];

		// Tempering
		x ^=  x >>> 11;
		x ^= ( x  <<  7 ) & 0x9d2c5680;
		x ^= ( x  << 15 ) & 0xefc60000;
		x ^=  x >>> 18;

		// Convert to unsigned
		return x >>> 0;
	};
	var api = {
		// [0,0xffffffff]
		"genrand_int32": () => randomInt32(),
		// [0,0x7fffffff]
		"genrand_int31": () => randomInt32() >>> 1,
		// [0,1]
		"genrand_real1": () => randomInt32() * ( 1.0 / 4294967295.0 ),
		// [0,1)
		"genrand_real2": () => randomInt32() * ( 1.0 / 4294967296.0 ),
		// (0,1)
		"genrand_real3": () => ( randomInt32() + 0.5 ) * ( 1.0 / 4294967296.0 ),
		// [0,1), 53-bit resolution
		"genrand_res53": () => {
			let a = randomInt32() >>> 5;
			let b = randomInt32() >>> 6;

			return ( a * 67108864.0 + b ) * ( 1.0 / 9007199254740992.0 );
		},

		"randomNumber": () => randomInt32(),
		"random31Bit": () => api.genrand_int31(),
		"randomInclusive": () => api.genrand_real1(),
		"random": () => api.genrand_real2(), // returns values just like Math.random
		"randomExclusive": () => api.genrand_real3(),
		"random53Bit": () => api.genrand_res53()
	};

	return api;
}

// Unique ID creation requires a high quality random # generator. In the browser we therefore
// require the crypto API and do not support built-in fallback to lower quality random number
// generators (like Math.random()).
var getRandomValues;
var rnds8 = new Uint8Array(16);
function rng() {
  // lazy load so that environments that need to polyfill have a chance to do so
  if (!getRandomValues) {
    // getRandomValues needs to be invoked in a context where "this" is a Crypto implementation. Also,
    // find the complete implementation of crypto (msCrypto) on IE11.
    getRandomValues = typeof crypto !== 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto) || typeof msCrypto !== 'undefined' && typeof msCrypto.getRandomValues === 'function' && msCrypto.getRandomValues.bind(msCrypto);

    if (!getRandomValues) {
      throw new Error('crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported');
    }
  }

  return getRandomValues(rnds8);
}

var REGEX = /^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i;

function validate(uuid) {
  return typeof uuid === 'string' && REGEX.test(uuid);
}

/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */

var byteToHex = [];

for (var i = 0; i < 256; ++i) {
  byteToHex.push((i + 0x100).toString(16).substr(1));
}

function stringify(arr) {
  var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  // Note: Be careful editing this code!  It's been tuned for performance
  // and works in ways you may not expect. See https://github.com/uuidjs/uuid/pull/434
  var uuid = (byteToHex[arr[offset + 0]] + byteToHex[arr[offset + 1]] + byteToHex[arr[offset + 2]] + byteToHex[arr[offset + 3]] + '-' + byteToHex[arr[offset + 4]] + byteToHex[arr[offset + 5]] + '-' + byteToHex[arr[offset + 6]] + byteToHex[arr[offset + 7]] + '-' + byteToHex[arr[offset + 8]] + byteToHex[arr[offset + 9]] + '-' + byteToHex[arr[offset + 10]] + byteToHex[arr[offset + 11]] + byteToHex[arr[offset + 12]] + byteToHex[arr[offset + 13]] + byteToHex[arr[offset + 14]] + byteToHex[arr[offset + 15]]).toLowerCase(); // Consistency check for valid UUID.  If this throws, it's likely due to one
  // of the following:
  // - One or more input array values don't map to a hex octet (leading to
  // "undefined" in the uuid)
  // - Invalid input values for the RFC `version` or `variant` fields

  if (!validate(uuid)) {
    throw TypeError('Stringified UUID is invalid');
  }

  return uuid;
}

function v4(options, buf, offset) {
  options = options || {};
  var rnds = options.random || (options.rng || rng)(); // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`

  rnds[6] = rnds[6] & 0x0f | 0x40;
  rnds[8] = rnds[8] & 0x3f | 0x80; // Copy bytes to buffer, if provided

  if (buf) {
    offset = offset || 0;

    for (var i = 0; i < 16; ++i) {
      buf[offset + i] = rnds[i];
    }

    return buf;
  }

  return stringify(rnds);
}

function clamp( source, min, max ){
	return Math.max( Math.min( max, source ), min );
}

function stringToNumberHash( input ){
	var strHash = 5381;
	var i = input.length;

	while( i ){
		strHash = ( strHash * 33 ) ^ input.charCodeAt( --i );
	}

	// Convert to unsigned int
	return strHash >>> 0;
}

function getSeed( seeds ){
	return seeds.reduce( ( seedling, seed, i ) => {
		let thisSeed = 0;

		if( Number.isInteger( seed ) ){
			thisSeed = seed;
		}
		else if( typeof seed == "string" ){
			thisSeed = stringToNumberHash( seed );
		}

		return seedling + ( seeds.length - i ) * thisSeed;
	}, 0 );
}


function getPseudoRandomNumberGenerator( ...seeds ){
	let seedNumber;

	if( seeds.length ){
		seedNumber = getSeed( seeds );
	}
	else {
		seedNumber = Math.floor( Math.random() * 10 ** 15 );
	}

	return MersenneTwister( seedNumber );
}

function randomValuesForUuid( prng ){
	var randomValues = [];

	for( let i = 0; i <= 3; i += 1 ){
		let buffer = new ArrayBuffer( 4 );
		let view = new DataView( buffer );

		view.setUint32( 0, prng.randomNumber() );

		randomValues.push(
			view.getUint8( 0 ),
			view.getUint8( 1 ),
			view.getUint8( 2 ),
			view.getUint8( 3 )
		);
	}

	return randomValues;
}


function uuid( {
	seeds = [],
	count = 1
} = {} ){
	var prng = getPseudoRandomNumberGenerator( ...seeds );
	var uuids = Array( clamp( count, 1, Number.MAX_SAFE_INTEGER ) )
		.fill( 0 )
		.map( () => v4( { "random": randomValuesForUuid( prng ) } ) );

	return count != 1 ? uuids : uuids[ 0 ];
}

export { clamp as c, uuid as u };
