var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, copyDefault, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && (copyDefault || key !== "default"))
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toCommonJS = /* @__PURE__ */ ((cache) => {
  return (module2, temp) => {
    return cache && cache.get(module2) || (temp = __reExport(__markAsModule({}), module2, 1), cache && cache.set(module2, temp), temp);
  };
})(typeof WeakMap !== "undefined" ? /* @__PURE__ */ new WeakMap() : 0);

// src/functions/submitFeedback.js
var submitFeedback_exports = {};
__export(submitFeedback_exports, {
  handler: () => handler
});
var import_https = require("https");

// src/js/common/node.js
function getEnvironmentVariable(name) {
  return process.env[name];
}

// config.js
var config = {
  "namespace": "feedback",
  "appId": "8d9fdab640c61ff0e8dcef3aebb0d329d1865f072a38166c2f29f80b65dd4549",
  "appSecretVar": {
    "type": "env",
    "name": "GITLAB_FEEDBACK_APP_SECRET"
  },
  "snippetTokenVar": {
    "type": "env",
    "name": "GITLAB_FEEDBACK_THOMAS_TOKEN"
  },
  "snippetId": 2253479,
  "feedbackFile": "feedback.txt",
  "extraEmails": [
    "gitlab@tomr.email"
  ]
};

// src/js/common/config.js
function getVariable({ type = "config", name, recurse = false } = {}) {
  var methods = {
    "config": (n) => config[n],
    "env": (n) => getEnvironmentVariable(n)
  };
  var result = methods[type](name);
  return recurse ? getVariable(result) : result;
}

// src/functions/submitFeedback.js
function getUser(token) {
  return new Promise((res) => {
    let resBody = [];
    let req = (0, import_https.request)("https://gitlab.com/api/v4/user", {
      "method": "GET",
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        "Authorization": `Bearer ${token}`
      }
    }, (result) => {
      result.on("data", (data) => {
        resBody.push(data);
      });
      result.on("end", () => {
        resBody = JSON.parse(Buffer.concat(resBody).toString());
        res(resBody);
      });
    });
    req.end();
  });
}
async function getCurrentFeedback() {
  var snippetId = getVariable({ "type": "config", "name": "snippetId" });
  var feedbackFile = getVariable({ "type": "config", "name": "feedbackFile" });
  return new Promise((res) => {
    let resBody = [];
    let req = (0, import_https.request)(`https://gitlab.com/api/v4/snippets/${snippetId}/files/main/${feedbackFile}/raw`, {
      "method": "GET",
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        "PRIVATE-TOKEN": getVariable({ "type": "config", "name": "snippetTokenVar", "recurse": true })
      }
    }, (result) => {
      result.on("data", (data) => {
        resBody.push(data);
      });
      result.on("end", () => {
        resBody = Buffer.concat(resBody).toString();
        res(resBody);
      });
    });
    req.end();
  });
}
async function updateFeedback(newFeedback) {
  var snippetId = getVariable({ "type": "config", "name": "snippetId" });
  var body = {
    "files": [
      {
        "action": "update",
        "file_path": getVariable({ "type": "config", "name": "feedbackFile" }),
        "content": newFeedback
      }
    ]
  };
  var bodyString = JSON.stringify(body);
  return new Promise((res) => {
    let resBody = [];
    let req = (0, import_https.request)(`https://gitlab.com/api/v4/snippets/${snippetId}`, {
      "method": "PUT",
      "body": bodyString,
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "PRIVATE-TOKEN": getVariable({ "type": "config", "name": "snippetTokenVar", "recurse": true })
      }
    }, (result) => {
      result.on("data", (data) => {
        resBody.push(data);
      });
      result.on("end", () => {
        resBody = JSON.parse(Buffer.concat(resBody).toString());
        res(resBody);
      });
    });
    req.write(bodyString, "utf8");
    req.end();
  });
}
async function addFeedback(content) {
  var currentFeedback = await getCurrentFeedback();
  var updatedFeedback = currentFeedback + content;
  return await updateFeedback(updatedFeedback);
}
function isGitLabTeamMember(user) {
  var isMember = user.email.endsWith("@gitlab.com") && user.confirmed_at;
  var otherAddress = getVariable({ "type": "config", "name": "extraEmails" }).includes(user.email);
  return isMember || otherAddress;
}
function craftFeedback(content, userSlug) {
  var now = new Date();
  return `

============================
At ${now.toISOString()}, ${userSlug} wrote:
----------------------------

${content}
`;
}
async function allowInternalOnly(content, anonymous, user) {
  var code = 403;
  var response = JSON.stringify({ "error": "Not a GitLab Team Member." });
  var userSlug = anonymous ? "an anonymous person" : `${user.name} (${user.email})`;
  if (isGitLabTeamMember(user)) {
    content = craftFeedback(content, userSlug);
    await addFeedback(content);
    code = 200;
    response = JSON.stringify({
      "submitted": true,
      content
    });
  }
  return {
    "statusCode": code,
    "body": response
  };
}
async function handler(event) {
  var { content, anonymous, token } = JSON.parse(event.body);
  var user = await getUser(token);
  return allowInternalOnly(content, anonymous, user);
}
module.exports = __toCommonJS(submitFeedback_exports);
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  handler
});
