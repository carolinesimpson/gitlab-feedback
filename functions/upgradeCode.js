var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, copyDefault, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && (copyDefault || key !== "default"))
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toCommonJS = /* @__PURE__ */ ((cache) => {
  return (module2, temp) => {
    return cache && cache.get(module2) || (temp = __reExport(__markAsModule({}), module2, 1), cache && cache.set(module2, temp), temp);
  };
})(typeof WeakMap !== "undefined" ? /* @__PURE__ */ new WeakMap() : 0);

// src/functions/upgradeCode.js
var upgradeCode_exports = {};
__export(upgradeCode_exports, {
  handler: () => handler
});
var import_https = require("https");

// src/js/common/node.js
function getEnvironmentVariable(name) {
  return process.env[name];
}

// config.js
var config = {
  "namespace": "feedback",
  "appId": "8d9fdab640c61ff0e8dcef3aebb0d329d1865f072a38166c2f29f80b65dd4549",
  "appSecretVar": {
    "type": "env",
    "name": "GITLAB_FEEDBACK_APP_SECRET"
  },
  "snippetTokenVar": {
    "type": "env",
    "name": "GITLAB_FEEDBACK_THOMAS_TOKEN"
  },
  "snippetId": 2253479,
  "feedbackFile": "feedback.txt",
  "extraEmails": [
    "gitlab@tomr.email"
  ]
};

// src/js/common/config.js
function getVariable({ type = "config", name, recurse = false } = {}) {
  var methods = {
    "config": (n) => config[n],
    "env": (n) => getEnvironmentVariable(n)
  };
  var result = methods[type](name);
  return recurse ? getVariable(result) : result;
}

// src/functions/upgradeCode.js
async function handler(event) {
  var body = JSON.parse(event.body);
  var config2 = {
    "url": "https://gitlab.com/oauth/token",
    "postBody": {
      "client_id": getVariable({ "type": "config", "name": "appId" }),
      "client_secret": getVariable({ "type": "config", "name": "appSecretVar", "recurse": true }),
      "code": body.code,
      "grant_type": "authorization_code",
      "redirect_uri": body.redirect
    }
  };
  var status = 400;
  var responseBody = JSON.stringify({ "error": "Unknown provider" });
  var auth = await new Promise((res) => {
    let resBody = [];
    let req = (0, import_https.request)(config2.url, {
      "method": "POST",
      "body": JSON.stringify(config2.postBody),
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
      }
    }, (result) => {
      result.on("data", (data) => {
        resBody.push(data);
      });
      result.on("end", () => {
        resBody = JSON.parse(Buffer.concat(resBody).toString());
        res(resBody);
      });
    });
    req.write(new URLSearchParams(config2.postBody).toString(), "utf8");
    req.end();
  });
  status = 200;
  responseBody = JSON.stringify(auth);
  return {
    "statusCode": status,
    "body": responseBody
  };
}
module.exports = __toCommonJS(upgradeCode_exports);
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  handler
});
