import { buildSync } from "esbuild";
import { globbySync } from "globby";

var inputs = globbySync( [
	"./public/**/index.js"
] );

buildSync( {
	"entryPoints": inputs,
	"bundle": true,
	"allowOverwrite": true,
	"splitting": true,
	"platform": "browser",
	"format": "esm",
	"assetNames": "[dir]/[name]",
	"outdir": "./public/js",
	"external": [
		"@thomasrandolph/taproot",
		"@thomasrandolph/taproot/Component",
		"@thomasrandolph/taproot/Function",
		"@thomasrandolph/taproot/MessageBus",
		"bowwow"
	]
} );