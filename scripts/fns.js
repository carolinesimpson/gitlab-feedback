import { buildSync } from "esbuild";
import { globbySync } from "globby";

var inputs = globbySync( [
	"./src/functions/**/*.js"
] );

buildSync( {
	"entryPoints": inputs,
	"bundle": true,
	"allowOverwrite": true,
	"splitting": false,
	"platform": "node",
	"format": "cjs",
	"assetNames": "[dir]/[name]",
	"outdir": "./functions",
	"external": [
		"@thomasrandolph/taproot",
		"@thomasrandolph/taproot/Component",
		"@thomasrandolph/taproot/Function",
		"@thomasrandolph/taproot/MessageBus",
		"bowwow"
	]
} );
