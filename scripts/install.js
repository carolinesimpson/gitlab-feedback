import { install } from "esinstall";

async function run(){
	await install(
		[
			"@thomasrandolph/taproot",
			"@thomasrandolph/taproot/Component",
			"@thomasrandolph/taproot/Function",
			"@thomasrandolph/taproot/MessageBus",
			"@thomasrandolph/taproot/Random",
			"@thomasrandolph/taproot/State",
			"@thomasrandolph/taproot/storage/local",
			"bowwow"
		],
		{
			"sourceMap": false,
			"dest": "./src/js/modules/",
			"treeshake": true
		}
	);
}

run();