import { sep } from "path";
import { rmSync } from "fs";

import { moveSync } from "fs-extra";
import { globbySync } from "globby";

var files = globbySync( [
	"./public/views/**"
] );
var prebundle;

files.forEach( ( file ) => {
	let dest = `${file.replace( `views${sep}`, "" )}`;

	moveSync( file, dest, {
		"overwrite": true
	} );
} );

[
	"./public/views",
	"./public/js/common",
	"./public/js/components"
].forEach( ( delPath ) => {
	rmSync( delPath, {
		"force": true,
		"recursive": true
	} );
} );

prebundle = globbySync( [
	"./public/**/index.js",
	"!./public/js/**"
] );

prebundle.forEach( ( js ) => {
	rmSync( js, { "force": true } );
} );