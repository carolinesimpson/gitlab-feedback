## Setup

1. Create an Application which requires `read_user` scope in your GitLab account.
	- Copy the Secret somewhere safe for now.
2. Create an access token in your GitLab account with `api` access.
    - Copy the token somewhere safe for now.
3. Decide on an environment variable name for the Application Secret
4. Decide on an environment variable name for the API Access Token.
5. Create a new snippet in your GitLab account with a file in it. This file will receive new feedback entries.
6. Push your fork to a hosted repository.
7. Create a new site on [Netlify](https://netlify.com) that deploys from the repository you pushed to in Step 6.
8. In your site's Build & Deploy section, add the two environment variables you decided on in Steps 3 and 4. Fill in the values you saved from Steps 1 and 2.
9. In your GitLab Application that you created in Step 1, update the Redirect URI field to be the deployed site name plus `/?provider=gitlab`.
	- For example, if your site is deployed to `vibrant-mihara-0358.netlify.app`, your Redirect URI field should be `https://vibrant-mihara-0358.netlify.app/?provider=gitlab`.
10. After updating your copy of `config.js` (see table below), push everything again to trigger a new Netlify deploy.

### `config.js`

| Key | Instructions |
| --- | ------------ |
| `appId` | Use the Application ID from the application you created in Step 1. |
| `appSecretVar` | Update the `name` property of this object to be the environment variable name you picked in Step 3. |
| `snippetTokenVar` | Update the `name` property of this object to be the environment variable name you picked in Step 4. |
| `snippetId` | Use the snippet ID that you created in Step 5. |
| `feedbackFile` | Use the filename of the file in the snippet you created in Step 5 |
| `extraEmails` | Update to allow feedback from non-GitLab team member accounts |
